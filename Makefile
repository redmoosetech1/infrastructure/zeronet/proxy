SHELL           := /bin/bash
IMAGE_NAME      = redmoosetech/squid
VERSION         = "$(shell date +%F)-$(shell git rev-parse --short HEAD)"

default: build

build:
	docker buildx use builder
	docker buildx build --platform=linux/amd64,linux/arm64 \
		-t $(IMAGE_NAME):$(VERSION) \
		-t $(IMAGE_NAME):latest --push .

update:
	kubectl set image deployment/squid-deployment squid=redmoosetech/squid:latest

deploy:
	kubectl apply -f deployment.yml

